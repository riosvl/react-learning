import React, {Component} from 'react';
import './App.css';
import Developer from "./Developer/Developer";
import Company from "./Company/Company";
import Header from "./Header/Header";
import Navigation from "./Navigation";
import DeveloperPage from "./DeveloperPage/DeveloperPage";
import Loader from "./Loader/Loader";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect, BrowserRouter
} from "react-router-dom";
import Axios from "axios";

class App extends Component{
  state = {
    some: '',
    changeStatus: false,
    title: 'Hello World!' ,
    styles: {
      textAlign: 'center',
      color: 'blue'
    },
    developer: [],
    showDevelopers: false,
    loading: true
  }

  /*changeTitle = (title) => {
    console.log('Changed')
    this.setState({title: title})
  }*/

  componentDidMount() {
    Axios.get('https://react-project-fca2c.firebaseio.com/developers.json').then((res) => {
      const developer = Object.values(res.data)
      this.setState({developer, loading: false})
    })

  }

  toggleDevelopersHandler = () => {
    this.setState({
      showDevelopers: !this.state.showDevelopers
    })
  }

  changeTitleHandler = (title) => {
    this.setState({title: title})
  }

  handleInput = (event) => {
    this.setState({
      title: event.target.value
    })
  }

  deleteDeveloper (index) {
    const developer = this.state.developer.concat()
    developer.splice(index, 1)
    this.setState({developer})
}

  createDeveloper(person) {
    const newDeveloper = this.state.developer.concat()
    newDeveloper.push({name: person.name, age: person.age})
    this.setState({developer: newDeveloper})

  }


  render() {
    return (
      <Router>
        <div>
          <Header/>
          <h1 style={this.state.styles}><b>{this.state.title}</b></h1>
          <input type="text" onChange={this.handleInput}/>
          <b><p style={{textAlign: 'center', color: 'pink', margin: '25px'}}>Эп, нот АП</p></b>
          <Switch>
            <Route exact path={'/'}>
              <h1>Hello</h1>
            </Route>
            <Route exact path='/company'><Company style={this.state.styles}/></Route>
            <Route exact path='/developer/:name'><DeveloperPage/></Route>
            <Route exact path='/developer'>
            <button style={{marginLeft: '48.5%'}} onClick={this.toggleDevelopersHandler}> Toggle</button>
            {this.state.loading ?
              <Loader />
              :
              this.state.developer.map((developer, index) => {
                  return (
                    this.state.showDevelopers ?
                    <Developer
                      name={developer.name}
                      age={developer.age}
                      onChangeName
                        changeTitle={this.changeTitleHandler.bind(this, developer.name)}
                        onDelete={this.deleteDeveloper.bind(this, index)}
                        //onCreate={this.createDeveloper.bind(this, developer)}
                    />
                    : null
                  )
                })
            }
            <button style={{marginLeft: '48.5%', marginTop: '5px'}}
                    onClick={this.createDeveloper.bind(this, {
                      name: 'Nikita',
                      age: 15
                    })}>
                    Create
            </button>
            </Route>
            <Route exact path={'*'}> <div style={{textAlign: 'center'}}>404</div> </Route>
          </Switch>
        </div>
      </Router>
    )
  }
}
export default App;
