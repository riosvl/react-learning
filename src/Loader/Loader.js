import React from "react";
import './Loader.css'

const Loader = props => (
  <div className="Loader">
    <div className="cssload-whirlpool">p</div>
  </div>
)

export default Loader