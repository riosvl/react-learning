import React, {Component} from "react";
import './Header.css'
import {NavLink} from "react-router-dom";

class Header extends Component{
  state = {
    some: ''
  }

  render() {
    let navLinkStyle = {
      color: ''
    }
    let navLinkStyleDeveloper = {
      color: ''
    }
    const url = window.location.href
    const actualUrl = url.split('/')
    const route = actualUrl[actualUrl.length-1]
    if (route === 'developer') {
      navLinkStyleDeveloper.color = 'red'
    }else if (route === 'company') {
      navLinkStyle.color = 'red'
    }

    return(
      <div className={'Header'}>
        <NavLink onClick={() => this.setState({some: 'privet'})} style={navLinkStyle} to="/company">Company </NavLink>
        <NavLink onClick={() => this.setState({some: 'zdraste'})} style={navLinkStyleDeveloper} to='/developer'>Developer</NavLink>
      </div>
    )
  }
}





export default Header;