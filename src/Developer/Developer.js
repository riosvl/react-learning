import React, {Component} from "react";
import Navigation from "../Navigation";
import Axios from "axios";


class Developer extends Component{



  goTo = (name) => {
    Navigation.push(`/developer/${name}`)
  }

  // constructor(props) {
  //   this.state = {
  //     name: '',
  //     age: '',
  //     isShowColor: false
  //   }
  //   this.setState({
  //     name: props.name,
  //     age: props.age,
  //     isShowColor: props.isShowColor
  //   })
  // }
  render() {
   return (

     <div className='Developer' style={{textAlign: 'center'}}>
       <h1 onClick={() => this.goTo(this.props.name)}>{this.props.name}</h1>
       <p>{this.props.age}</p>
       <input onChange={this.props.onChangeName}/>
       <button onClick={this.props.changeTitle}>IButton</button>
       <button onClick={this.props.onDelete}>Delete</button>
     </div>
   )
 }
}

export default Developer
