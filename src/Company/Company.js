import React, {Component} from 'react';

class Company extends Component{

  state = {
    companyAge: '5 years of work',
    companyStatus: 'active',
    amountOfWorkers: 1000
  }



  render(){
    return(
      <div style={this.props.style}>
        <p>{this.state.amountOfWorkers}</p>
        <p>{this.state.companyAge}</p>
        <p>{this.state.companyStatus}</p>
      </div>
    )
  }
}

export default Company